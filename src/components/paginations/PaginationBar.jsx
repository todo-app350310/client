import { Pagination } from "@shopify/polaris";
import React from "react";

export default function PaginationBar() {
  return (
    <div className="pagination-bar">
      <Pagination
        onPrevious={() => {
          console.log("Previous");
        }}
        onNext={() => {
          console.log("Next");
        }}
        type="page"
        hasNext
        label="1-50 of 8,450 orders"
      />
    </div>
  );
}
