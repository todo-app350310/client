import { Modal, TextField } from "@shopify/polaris";
import React, { useCallback, useState } from "react";

const CreateModal = (props) => {
  const { open, handleClose, handleAdd } = props;
  const [value, setValue] = useState(null);
  const handleChangeValue = useCallback((newValue) => setValue(newValue), []);
  return (
    <Modal
      open={open}
      onClose={handleClose}
      title="Create todo"
      primaryAction={{
        content: "Add",
        onAction: () => handleAdd(value),
      }}
      secondaryActions={[
        {
          content: "Cancel",
          onAction: handleClose,
        },
      ]}
    >
      <Modal.Section>
        <TextField
          label="Title"
          value={value}
          onChange={handleChangeValue}
          autoComplete="off"
        />
      </Modal.Section>
    </Modal>
  );
};

export default CreateModal;
