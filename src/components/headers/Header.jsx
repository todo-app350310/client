import React from "react";
import "./style.css";
import ShoppingBag from "../../assets/images/shopping-bag.png";
import ShoppingText from "../../assets/images/shopping-text.png";
import SearchBar from "../searchBars/SearchBar";
import { InlineGrid } from "@shopify/polaris";
import RightHeader from "./RightHeader";

const Header = () => {
  return (
    <div className="wrap-header">
      <div className="logo">
        <img src={ShoppingBag} alt="shopping-bag" className="logo-image" />
        <img src={ShoppingText} alt="shopping-text" className="logo-image" />
      </div>
      <div className="wrap-search-bar">
        <SearchBar />
      </div>
      <div className="wrap-right-header">
        <RightHeader />
      </div>
    </div>
  );
};

export default Header;
