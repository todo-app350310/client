import React from "react";
import { Avatar, Box } from "@shopify/polaris";
import avatarImage from "../../assets/images/avatar.png";

const RightHeader = () => {
  return (
    <div style={{ display: "flex", flexDirection: "row", gap: "10px" }}>
      <Box>
        <Avatar customer name="Farrah" source={avatarImage} />
      </Box>
      ;
    </div>
  );
};

export default RightHeader;
