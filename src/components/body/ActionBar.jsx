import { Button } from "@shopify/polaris";
import "./style.css";

const ActionBar = (props) => {
  const { handleDeleteItems, handleUpdateItems } = props;
  return (
    <div className="flex-space-center wrap-action-bar">
      <div className="action-bar">
        <Button onClick={(e) => handleUpdateItems(e, "complete")} pressed>
          Complete
        </Button>
        <Button onClick={(e) => handleUpdateItems(e, "incomplete")} pressed>
          InComplete
        </Button>
        <Button onClick={handleDeleteItems} tone="critical" pressed>
          Delete
        </Button>
      </div>
    </div>
  );
};
export default ActionBar;
