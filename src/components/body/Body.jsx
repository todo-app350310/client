import { ResourceList, ResourceItem, Text, Button } from "@shopify/polaris";
import { useEffect, useState } from "react";
import "./style.css";
import PaginationBar from "../paginations/PaginationBar";
import ActionBar from "./ActionBar";
import NavBar from "./NavBar";
import {
  createItem,
  deleteItem,
  deleteItems,
  getTodoListApi,
  updateItem,
  updateItems,
} from "../../apis";

export default function Body() {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const fetchTodoList = async () => {
    try {
      const res = await getTodoListApi(null);
      console.log(res);
      if (res?.data) {
        setItems(res.data?.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchTodoList();
  }, []);

  const handleUpdateItem = async (e, id, status) => {
    setLoading(true);
    try {
      const res = await updateItem(
        { id },
        { status: status === "complete" ? 1 : 2 }
      );
      if (res.status === 200) {
        fetchTodoList();
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const handleUpdateItems = async (e, status) => {
    try {
      const res = await updateItems({
        ids: selectedItems,
        status: status === "complete" ? 1 : 2,
      });
      console.log("adaad", res.status);
      if (res.status === 200) {
        setSelectedItems([]);
        fetchTodoList();
      }
    } catch (error) {
      setLoading(false);
    }
  };
  const handleDeleteItem = async (e, id) => {
    setLoading(true);
    try {
      const res = await deleteItem({ id });
      if (res.status === 200) {
        fetchTodoList();
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const handleDeleteItems = async () => {
    try {
      const res = await deleteItems(selectedItems);
      if (res.status === 200) {
        setSelectedItems([]);
        fetchTodoList();
      }
    } catch (error) {}
  };
  const resourceName = {
    singular: "customer",
    plural: "customers",
  };

  const handleAdd = async (title) => {
    try {
      const res = await createItem({
        title,
      });
      fetchTodoList();
    } catch (error) {}
  };

  return (
    <div className="wrap-body">
      <NavBar handleAdd={handleAdd} />
      <ResourceList
        resourceName={resourceName}
        items={items}
        renderItem={renderItem}
        selectedItems={selectedItems}
        onSelectionChange={setSelectedItems}
        selectable
      />
      {selectedItems?.length > 0 && (
        <ActionBar
          handleDeleteItems={handleDeleteItems}
          handleUpdateItems={handleUpdateItems}
        />
      )}
      {/* <PaginationBar /> */}
    </div>
  );

  function renderItem(item) {
    const { id, title, status } = item;

    return (
      <ResourceItem
        id={id}
        url={""}
        accessibilityLabel={`View details for ${title}`}
      >
        <div className="flex-space-between">
          <Text variant="bodyMd" fontWeight="bold" as="h3">
            {title}
          </Text>
          <div className="flex-space-between wrap-item-action">
            {status === 1 ? (
              <div className="complete-box">Complete</div>
            ) : (
              <div className="incomplete-box">Incomplete</div>
            )}
            {status === 1 ? (
              <Button
                loading={loading}
                onClick={(e) => handleUpdateItem(e, id, "incomplete")}
              >
                Incomplete
              </Button>
            ) : (
              <Button
                loading={loading}
                onClick={(e) => handleUpdateItem(e, id, "complete")}
              >
                Complete
              </Button>
            )}
            <Button
              tone="critical"
              loading={loading}
              onClick={(e) => handleDeleteItem(e, id)}
            >
              Delete
            </Button>
          </div>
        </div>
      </ResourceItem>
    );
  }
}
