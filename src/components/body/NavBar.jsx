import { Button, Modal, Text } from "@shopify/polaris";
import React, { useState } from "react";
import CreateModal from "../modals/CreateModal";

const NavBar = (props) => {
  const { handleAdd } = props;
  const [open, setOpen] = useState(false);
  const handleOpenCreateModal = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className="flex-space-between nav-bar">
      <p
        style={{
          fontWeight: 650,
          fontSize: "20px",
          lineHeight: "24px",
          color: "#303030",
        }}
      >
        Todoes
      </p>
      <Button variant="primary" onClick={handleOpenCreateModal}>
        Create
      </Button>
      {open && (
        <CreateModal
          open={open}
          handleClose={handleClose}
          handleAdd={handleAdd}
        />
      )}
    </div>
  );
};

export default NavBar;
