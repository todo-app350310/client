import axios from "axios";

const fetchApi = async (endPoint, method, params, formdata) => {
  const BASE_URL = process.env.REACT_APP_BASE_URL;
  let configs = {
    url: `${BASE_URL}/api/${endPoint}`,
    method: method,
    params,
    data: formdata,
  };

  let response = await axios(configs);
  return response;
};

export const getTodoListApi = async (params) => {
  return fetchApi("items", "GET", params, null);
};

export const updateItem = async (params, payload) => {
  return fetchApi(`items/${params.id}`, "PUT", null, payload);
};
export const updateItems = async (payload) => {
  return fetchApi(`items`, "PUT", null, payload);
};

export const createItem = async (payload) => {
  return fetchApi(`items`, "POST", null, payload);
};

export const deleteItem = async (params) => {
  return fetchApi(`items/${params.id}`, "DELETE", null, null);
};
export const deleteItems = async (payload) => {
  return fetchApi(`items`, "DELETE", null, payload);
};